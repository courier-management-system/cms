const db = require("../models");
const Invoice = db.invoice;
const InvoiceItems = db.invoiceItems;

exports.getInvoices = async (req, res) => {
    try {
        let invoices = await Invoice.findAll({
            include: [{
                model: InvoiceItems
            }]
        });
        res.status(200).send({ data: invoices, statusCode: 200 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.addInvoice = async (req, res) => {
    try {
        let invoice = await Invoice.create({
            invoiceNumber: req.body.invoiceNumber,
            customerName: req.body.customerName,
            customerGst: req.body.customerGst,
            address: req.body.address,
            state: req.body.state,
            sgstPercentage: req.body.sgstPercentage,
            sgst: req.body.sgst,
            cgstPercentage: req.body.cgstPercentage,
            cgst: req.body.cgst,
            igstPercentage: req.body.igstPercentage,
            igst: req.body.igst,
            totalAmount: req.body.totalAmount,
        });

        if (invoice) {
            req.body.invoiceItems?.forEach(async item => {
                await InvoiceItems.create({
                    vphNumber: item.vphNumber,
                    consignor: item.consignor,
                    fromCity: item.fromCity,
                    toCity: item.toCity,
                    weight: item.weight,
                    rate: item.rate,
                    courierDate: item.courierDate,
                    amount: item.amount,
                    description: item.description,
                    invoiceId: invoice.id,
                });
            });
        }
        res.status(201).send({ message: 'Invoice generated successfully', statusCode: 201 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.updateInvoice = async (req, res) => {
    try {
        let invoice = await Invoice.update({
            invoiceNumber: req.body.invoiceNumber,
            customerName: req.body.customerName,
            customerGst: req.body.customerGst,
            address: req.body.address,
            state: req.body.state,
            sgstPercentage: req.body.sgstPercentage,
            sgst: req.body.sgst,
            cgstPercentage: req.body.cgstPercentage,
            cgst: req.body.cgst,
            igstPercentage: req.body.igstPercentage,
            igst: req.body.igst,
            totalAmount: req.body.totalAmount,
        }, { where: { id: req.params.id } });

        if (invoice) {
            await InvoiceItems.destroy({ where: { invoiceId: req.params.id } });
            req.body.invoiceItems?.forEach(async item => {
                await InvoiceItems.create({
                    vphNumber: item.vphNumber,
                    consignor: item.consignor,
                    fromCity: item.fromCity,
                    toCity: item.toCity,
                    weight: item.weight,
                    rate: item.rate,
                    courierDate: item.courierDate,
                    amount: item.amount,
                    description: item.description,
                    invoiceId: req.params.id,
                });
            });
        }
        res.status(201).send({ message: 'Invoice generated successfully', statusCode: 201 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.deleteInvoice = async (req, res) => {
    try {
        let invoice = await Invoice.findByPk(req.params.id);
        if (invoice) {
            await Invoice.destroy({ where: { id: req.params.id } });
            res.status(200).send({ message: 'Invoice deleted successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Invoice does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};
