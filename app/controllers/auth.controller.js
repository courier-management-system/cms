const db = require("../models");
const config = require("../config/auth.config");
const User = db.admin;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

exports.signin = async (req, res) => {
    try {
        const user = await User.findOne({
            where: {
                email: req.body.email,
            },
        });

        if (!user) {
            return res.status(404).send({ message: "User Not found.", statusCode: 404 });
        }

        const passwordIsValid = bcrypt.compareSync(
            req.body.password,
            user.password
        );

        if (!passwordIsValid) {
            return res.status(401).send({
                message: "Invalid Password!",
                statusCode: 401
            });
        }

        const token = jwt.sign({ id: user.id }, config.secret, {
            expiresIn: 86400, // 24 hours
        });

        req.session.token = token;

        return res.status(200).send({
            id: user.id,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            accessToken: token
        });
    } catch (error) {
        return res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.signout = async (req, res) => {
    try {
        req.session = null;
        return res.status(200).send({
            message: "You've been signed out!",
            statusCode: 200
        });
    } catch (err) {
        this.next(err);
    }
};