const db = require("../models");
const utils = require('./../utils/common');

const Courier = db.courier;
const Client = db.client;
const Train = db.train;
const Payment = db.payment;
const Ledger = db.ledger;


// exports.getPaymentLedger = async (req, res) => {
//     try {
//         let ledger = [];
//         let clients = await Client.findAll({
//             raw: true, where: {
//                 isDeleted: false
//             }
//         });
//         let paymentHistory = await getPaymentData();
//         clients?.forEach(client => {
//             ledger.push({
//                 client: client,
//                 paymentHistory: paymentHistory?.filter(payment => payment.clientId === client.id)
//             });
//         });
//         res.status(200).send({ data: ledger, statusCode: 200 });
//     } catch (error) {
//         res.status(500).send({ message: error.message, statusCode: 500 });
//     }
// }


exports.getPaymentLedger = async (req, res) => {
    try {
        let ledger = [];
        let clients = await Client.findAll({
            raw: true,
            where: {
                isDeleted: false
            }
        });
        let paymentHistory = await Ledger.findAll({
            raw: true
        })
        clients?.forEach(client => {
            ledger.push({
                client: client,
                paymentHistory: paymentHistory?.filter(payment => payment.clientId === client.id)
            });
        });
        res.status(200).send({ data: ledger, statusCode: 200 });
    } catch (err) {
        res.status(500).send({ message: err.message, statusCode: 500 });
    }
};


exports.getAllPaymentDetail = async (req, res) => {
    try {
        let payments = await Courier.findAll({
            include: [{ model: Client }, { model: Train }]
        });
        payments = payments?.filter(payment => payment.totalAmount > 0 && payment.balanceAmount > 0 && !payment.isDeleted);
        let paymentData = [];
        payments?.forEach(payment => {
            let obj = paymentData?.find(p => p.clientId === payment.clientId);
            if (obj) {
                obj['totalAmount'] = obj['totalAmount'] + payment.totalAmount;
                obj['paidAmount'] = obj['paidAmount'] + payment.paidAmount;
                obj['balanceAmount'] = obj['balanceAmount'] + payment.balanceAmount;
            } else {
                paymentData.push({
                    clientId: payment.clientId,
                    client: payment.client,
                    totalAmount: payment.totalAmount,
                    balanceAmount: payment.balanceAmount,
                    paidAmount: payment.paidAmount
                });
            }
        });
        res.status(200).send({ data: paymentData, statusCode: 200 });
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.updatePaymentDetail = async (req, res) => {
    try {
        let amountToPay = req.body.amountToPay || 0;
        let courier = await Courier.findAll({
            where: {
                clientId: req.body.clientId
            },
            raw: true,
            order: [['clientId', 'ASC']]
        });
        if (req.body.amountToPay > 0 && courier) {
            courier.forEach(async (detail) => {
                if (detail.balanceAmount > 0 && amountToPay > 0 && !detail.isDeleted) {
                    if (detail.balanceAmount < amountToPay) {
                        let paidAmount = detail.paidAmount + detail.balanceAmount;
                        amountToPay = amountToPay - detail.balanceAmount;
                        await Courier.update(
                            {
                                paidAmount: paidAmount,
                                balanceAmount: 0
                            },
                            { where: { id: detail.id } }
                        );
                        let paymentHist = {
                            totalAmount: detail.totalAmount, paidAmount,
                            balanceAmount: 0, remark: req.body.remark, paymentDate: req.body.paymentDate,
                            courierId: detail.id
                        };
                        await utils.addPaymentHistory(paymentHist);
                    } else {
                        let balanceAmount = detail.balanceAmount - amountToPay;
                        let paidAmount = detail.paidAmount + amountToPay;
                        amountToPay = 0;
                        await Courier.update(
                            {
                                paidAmount: paidAmount,
                                balanceAmount: balanceAmount
                            },
                            { where: { id: detail.id } });

                        let paymentHist = {
                            totalAmount: detail.totalAmount, paidAmount,
                            balanceAmount, remark: req.body.remark, paymentDate: req.body.paymentDate,
                            courierId: detail.id
                        };
                        await utils.addPaymentHistory(paymentHist);
                    }
                }
            });
            await utils.addLedger({
                creditAmount: req.body.amountToPay, remark: req.body.remark,
                paymentDate: req.body.paymentDate, clientId: req.body.clientId
            });
            res.status(200).send({ message: "Payment updated successfully", statusCode: 200 });
        } else {
            res.status(400).send({ message: "Pay amount should be grater than 0", statusCode: 400 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};


async function getPaymentData() {
    let paymentHist = [];
    let couriers = await Courier.findAll({
        where: {
            isDeleted: false,
        },
        raw: true
    });
    let payments = await Payment.findAll({
        where: {
            isDeleted: false,
        },
        raw: true
    });
    couriers?.forEach(async courier => {
        let payment = payments.filter(payment => payment.courierId === courier.id);
        console.log(payment)
        paymentHist.push({
            ...courier,
            payment: payment
        });
    });
    return paymentHist;
}


