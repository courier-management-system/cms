const db = require("../models");
const Expense = db.expense;

exports.allExpenses = async (req, res) => {
    try {
        let expense = await Expense.findAll();
        res.status(200).send({ data: expense, statusCode: 200 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.getExpenseById = async (req, res) => {
    try {
        let expense = await Expense.findByPk(req.params.id);
        if (expense) {
            res.status(200).send({ data: expense, message: '', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Expense does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.addExpense = async (req, res) => {
    try {
        await Expense.create({
            type: req.body.type,
            description: req.body.description,
            amount: req.body.amount,
            date: req.body.date
        });
        res.status(201).send({ message: 'Expense added successfully', statusCode: 201 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.updateExpense = async (req, res) => {
    try {
        let expense = await Expense.findOne({
            where: {
                id: req.params.id,
            },
        });
        if (expense) {
            await Expense.update({
                type: req.body.type,
                description: req.body.description,
                amount: req.body.amount,
                date: req.body.date
            }, { where: { id: req.params.id } });
            expense = await Expense.findOne({
                where: {
                    id: req.params.id,
                },
            });
            res.status(200).send({ data: expense, message: 'Expense detail updated successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Expense does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.deleteExpense = async (req, res) => {
    try {
        let expense = await Expense.findByPk(req.params.id);
        if (expense) {
            await Expense.update({ isDeleted: true }, { where: { id: req.params.id } });
            res.status(200).send({ data: expense, message: 'Expense delted successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Expense does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.restoreExpense = async (req, res) => {
    try {
        let expense = await Expense.findByPk(req.query.id);
        if (expense) {
            await Expense.update({ isDeleted: false }, { where: { id: req.query.id } });
            res.status(200).send({ data: expense, message: 'Expense restored successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Expense does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

