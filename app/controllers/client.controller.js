const db = require("../models");
const Client = db.client;

exports.getClients = async (req, res) => {
    try {
        let client = await Client.findAll();
        res.status(200).send({ data: client, statusCode: 200 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.getClientById = async (req, res) => {
    try {
        let client = await Client.findByPk(req.params.id);
        if (client) {
            res.status(200).send({ data: client, statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Cleint does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.addClient = async (req, res) => {
    try {
        await Client.create({
            companyName: req.body.companyName,
            email: req.body.email,
            contact: req.body.contact,
            address: req.body.address,
            city: req.body.city,
            state: req.body.state,
            pincode: req.body.pincode,
            gstNo: req.body.gstNo,
        });
        res.status(201).send({ message: "client registered successfully!", statusCode: 201 });
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.updateClient = async (req, res) => {
    try {
        let client = await Client.findOne({
            where: {
                id: req.params.id,
            },
        });
        if (client) {
            await Client.update({
                companyName: req.body.companyName,
                email: req.body.email,
                contact: req.body.contact,
                address: req.body.address,
                city: req.body.city,
                state: req.body.state,
                pincode: req.body.pincode,
                gstNo: req.body.gstNo,
            }, { where: { id: req.params.id } });
            client = await Client.findOne({
                where: {
                    id: req.params.id,
                },
            });
            res.status(200).send({ data: client, message: 'Cleint detail updated successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Cleint does does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.deleteClient = async (req, res) => {
    try {
        let client = await Client.findByPk(req.params.id);
        if (client) {
            await Client.update({ isDeleted: true }, { where: { id: req.params.id } });
            res.status(200).send({ data: client, message: 'Cleint delted successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Cleint does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.restoreClient = async (req, res) => {
    try {
        let client = await Client.findByPk(req.query.id);
        if (client) {
            await Client.update({ isDeleted: false }, { where: { id: req.query.id } });
            res.status(200).send({ data: client, message: 'Cleint restored successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Cleint does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};


