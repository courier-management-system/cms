const db = require("../models");
const Train = db.train;

exports.getTrains = async (req, res) => {
    // get Train from Database
    try {
        let trains = await Train.findAll();
        res.status(200).send({ data: trains, statusCode: 200 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.getTrainById = async (req, res) => {
    // get Train from Database
    try {
        let train = await Train.findByPk(req.params.id);
        if (train) {
            res.status(200).send({ data: train, message: '', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Train does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.addTrain = async (req, res) => {
    // Save Train to Database
    try {
        await Train.create({
            trainNumber: req.body.trainNumber,
            toStation: req.body.toStation,
            fromStation: req.body.fromStation
        });
        res.status(201).send({ message: 'Train added successfully', statusCode: 201 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.updateTrain = async (req, res) => {
    // get Train from Database
    try {
        let train = await Train.findOne({
            where: {
                id: req.params.id,
            },
        });
        if (train) {
            await Train.update({
                trainNumber: req.body.trainNumber,
                toStation: req.body.toStation,
                fromStation: req.body.fromStation
            }, { where: { id: req.params.id } });
            train = await Train.findOne({
                where: {
                    id: req.params.id,
                },
            });
            res.status(200).send({ data: train, message: 'Train detail updated successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Train does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.deleteTrain = async (req, res) => {
    // get Train from Database
    try {
        let train = await Train.findByPk(req.params.id);
        if (train) {
            await Train.update({ isDeleted: true }, { where: { id: req.params.id } });
            res.status(200).send({ data: train, message: 'Train delted successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Train does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.restoreTrain = async (req, res) => {
    // get Train from Database
    try {
        let train = await Train.findByPk(req.query.id);
        if (train) {
            await Train.update({ isDeleted: false }, { where: { id: req.query.id } });
            res.status(200).send({ data: train, message: 'Train restored successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Train does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

