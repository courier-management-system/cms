const db = require("../models");
const utils = require('./../utils/common');

const Courier = db.courier;
const Client = db.client;
const Vehicle = db.train;

exports.getAllCourierDetail = async (req, res) => {
    try {
        let couriers = await Courier.findAll(
            {
                include: [{
                    model: Client
                }, {
                    model: Vehicle
                }]
            }
        );
        res.status(200).send({ data: couriers, statusCode: 200 });

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.getCourierDetailById = async (req, res) => {
    try {
        let couriers = await Courier.findByPk(req.params.id,
            {
                include: [{
                    model: Client
                }, {
                    model: Vehicle
                }]
            });
        if (couriers) {
            res.status(200).send({ data: couriers, message: '', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Courier does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.addCourierDetail = async (req, res) => {
    try {
        if (req.body.totalAmount === (req.body.paidAmount + req.body.balanceAmount)) {
            const courier = await Courier.create({
                vphNumber: req.body.vphNumber,
                packetNumber: req.body.packetNumber,
                description: req.body.description,
                type: req.body.type,
                weight: req.body.weight,
                weightType: req.body.weightType,
                rate: req.body.rate,
                courierDate: req.body.courierDate,
                totalAmount: req.body.totalAmount,
                paidAmount: req.body.paidAmount,
                balanceAmount: req.body.balanceAmount,
                paymentType: req.body.paymentType,
                remark: req.body.remark,
                trainId: req.body.trainId,
                clientId: req.body.clientId
            });

            let paymentHist = {
                totalAmount: req.body.totalAmount,
                paidAmount: req.body.paidAmount,
                balanceAmount: req.body.balanceAmount,
                remark: req.body.remark,
                paymentDate: req.body.courierDate,
                courierId: courier.id
            };
            await utils.addPaymentHistory(paymentHist);
            await utils.addLedger({
                creditAmount: req.body.paidAmount, remark: req.body.remark,
                paymentDate: req.body.courierDate, clientId: req.body.clientId
            });

            res.status(201).send({ message: 'Courier added successfully', statusCode: 201 });
        } else {
            res.status(400).send({ message: 'Total amount should be sum of balance and paid amount', statusCode: 400 });
        }

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.updateCourierDetail = async (req, res) => {
    try {
        if (req.body.totalAmount === (req.body.paidAmount + req.body.balanceAmount)) {

            let courier = await Courier.findOne({
                raw: true,
                where: {
                    id: req.params.id,
                },
            });
            if (courier) {
                await Courier.update({
                    vphNumber: req.body.vphNumber,
                    packetNumber: req.body.packetNumber,
                    description: req.body.description,
                    type: req.body.type,
                    weight: req.body.weight,
                    weightType: req.body.weightType,
                    rate: req.body.rate,
                    courierDate: req.body.courierDate,
                    totalAmount: req.body.totalAmount,
                    paidAmount: req.body.paidAmount,
                    balanceAmount: req.body.balanceAmount,
                    paymentType: req.body.paymentType,
                    remark: req.body.remark,
                    trainId: req.body.trainId,
                    clientId: req.body.clientId
                }, { where: { id: req.params.id } });

                let paymentHist = {
                    totalAmount: req.body.totalAmount,
                    paidAmount: req.body.paidAmount,
                    balanceAmount: req.body.balanceAmount,
                    remark: req.body.remark,
                    paymentDate: req.body.courierDate,
                    courierId: req.params.id
                };
                await utils.updatePaymentHistory(paymentHist, req.params.id);
                await utils.updateLedger({
                    creditAmount: req.body.paidAmount, remark: req.body.remark,
                    paymentDate: req.body.courierDate, clientId: req.body.clientId
                });
                courier = await Courier.findOne({
                    where: {
                        id: req.params.id,
                    },
                });
                res.status(200).send({ data: courier, message: 'Courier detail updated successfully', statusCode: 200 });
            } else {
                res.status(400).send({ message: 'Total amount should be sum of balance and paid amount', statusCode: 400 });
            }
        } else {
            res.status(404).send({ message: 'Courier detail does does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.deleteCourierDetail = async (req, res) => {
    try {
        let courier = await Courier.findByPk(req.params.id);
        if (courier) {
            await Courier.update({ isDeleted: true }, { where: { id: req.params.id } });
            res.status(200).send({ data: courier, message: 'Courier delted successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Courier detail does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.restoreCourierDetail = async (req, res) => {
    try {
        let courier = await Courier.findByPk(req.query.id);
        if (courier) {
            await Courier.update({ isDeleted: false }, { where: { id: req.query.id } });
            res.status(200).send({ data: courier, message: 'Courier restored successfully', statusCode: 200 });
        } else {
            res.status(404).send({ message: 'Courier detail does not exist', statusCode: 404 });
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};


