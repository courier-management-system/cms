const db = require("../models");

const Courier = db.courier;
const Payment = db.payment;

exports.addOrderDetail = async (req, res) => {
    try {
        let courier = await Courier.create({
            description: req.body.description,
            type: req.body.type,
            vphNumber: req.body.vphNumber,
            weight: req.body.weight,
            weightType: req.body.weightType,
            rate: req.body.rate,
            date: req.body.date,
            clientId: req.body.clientId,
            trainId: req.body.trainId
        });
        if (courier?.dataValues?.id) {
            await Payment.create({
                totalAmount: req.body.totalAmount,
                paidAmount: req.body.paidAmount,
                balanceAmount: req.body.balanceAmount,
                paymentType: req.body.paymentType,
                paymentDate: req.body.paymentDate,
                remark: req.body.remark,
                courierId: courier?.dataValues?.id,
            });
            res.status(201).send({ message: 'Order added successfully', statusCode: 201 });
        }

    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

exports.updateOrderDetail = async (req, res) => {
    try {
        let courier = await Courier.findOne({
            where: {
                id: req.query.courierId,
            },
        });
        if (courier) {
            await Courier.update({
                description: req.body.description,
                type: req.body.type,
                vphNumber: req.body.vphNumber,
                weight: req.body.weight,
                weightType: req.body.weightType,
                rate: req.body.rate,
                date: req.body.date,
                clientId: req.body.clientId,
                trainId: req.body.trainId
            }, { where: { id: req.query.courierId } });

            let payment = await Payment.findOne({
                where: {
                    id: req.query.paymentId
                },
            });
            if (payment) {
                await Payment.update({
                    totalAmount: req.body.totalAmount,
                    paidAmount: req.body.paidAmount,
                    balanceAmount: req.body.balanceAmount,
                    paymentType: req.body.paymentType,
                    paymentDate: req.body.paymentDate,
                    remark: req.body.remark,
                    courierId: req.query.courierId,
                }, { where: { id: req.query.paymentId } });
                res.status(200).send({ message: 'Order detail updated successfully', statusCode: 200 });
            }
        }
    } catch (error) {
        res.status(500).send({ message: error.message, statusCode: 500 });
    }
};

