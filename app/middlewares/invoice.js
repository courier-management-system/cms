const db = require("../models");
const Invoice = db.invoice;

checkDuplicateInvoiceNumber = async (req, res, next) => {
    try {
        let invoice = await Invoice.findOne({
            where: {
                invoiceNumber: req.body.invoiceNumber
            }
        });

        if (invoice) {
            return res.status(400).send({
                message: "Failed! Invoice number already exist"
            });
        }

        next();
    } catch (error) {
        return res.status(500).send({
            message: "Unable to validate!"
        });
    }
};

const verifyInvoiceNumber = {
    checkDuplicateInvoiceNumber
};

module.exports = verifyInvoiceNumber;
