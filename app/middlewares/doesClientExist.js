const db = require("../models");
const Client = db.client;

checkDuplicateCompanyName = async (req, res, next) => {
    try {
        let client = await Client.findOne({
            where: {
                email: req.body.companyName
            }
        });

        if (client) {
            return res.status(400).send({
                message: "Failed! Companu name is already in use!"
            });
        }

        next();
    } catch (error) {
        return res.status(500).send({
            message: "Unable to validate Username!"
        });
    }
};

const verifyCompanyName = {
    checkDuplicateCompanyName
};

module.exports = verifyCompanyName;
