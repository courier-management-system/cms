const db = require("../models");
const Courier = db.courier;

checkDuplicateCourierNumber = async (req, res, next) => {
    try {
        let client = await Courier.findOne({
            where: {
                vphNumber: req.body.vphNumber
            }
        });

        if (client) {
            return res.status(400).send({
                message: "Failed! VPH number already exist"
            });
        }

        next();
    } catch (error) {
        return res.status(500).send({
            message: "Unable to validate!"
        });
    }
};

const verifyCourierNumber = {
    checkDuplicateCourierNumber
};

module.exports = verifyCourierNumber;
