const db = require("../models");
const Train = db.train;

doesTrainExist = async (req, res, next) => {
    try {
        const train = await Train.findOne({
            where: {
                trainNumber: req.body.trainNumber
            }
        });

        if (train) {
            return res.status(403).send({
                message: "Train is already present",
                statusCode: 403
            });
        }
        return next();
    } catch (error) {
        return res.status(500).send({
            message: "Something went wrong, please try again",
            statusCode: 500
        });
    }
};

module.exports = {
    doesTrainExist
};
