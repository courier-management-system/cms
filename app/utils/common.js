const db = require("../models");
const Payment = db.payment;
const Ledger = db.ledger;


exports.addPaymentHistory = async (req) => {
    try {
        await Payment.create({
            ...req
        });
    } catch (e) {
        console.log(e);
    }
};

exports.updatePaymentHistory = async (req) => {
    try {
        await Payment.destroy({ where: { courierId: req.courierId } });
        await Payment.create({
            ...req
        });
    } catch (e) {
        console.log(e);
    }
};

exports.addLedger = async (req) => {
    try {
        await Ledger.create({
            ...req
        });
    } catch (e) {
        console.log(e);
    }
};

exports.updateLedger = async (req) => {
    try {
        await Ledger.destroy({ where: { clientId: req.clientId } });
        await Ledger.create({
            ...req
        });
    } catch (e) {
        console.log(e);
    }
};