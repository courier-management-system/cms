const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,
    {
        host: config.HOST,
        dialect: config.dialect,
        operatorsAliases: false,

        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        }
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.admin = require("./admin.model")(sequelize, Sequelize);
db.client = require("./client.model")(sequelize, Sequelize);
db.train = require('./train.model')(sequelize, Sequelize);
db.courier = require('../models/courier.model')(sequelize, Sequelize);
db.expense = require('../models/expense.model')(sequelize, Sequelize);
db.payment = require('../models/payment.model')(sequelize, Sequelize);
db.ledger = require('../models/ledger.modal')(sequelize, Sequelize);
db.invoice = require('../models/invoice.model')(sequelize, Sequelize);
db.invoiceItems = require('../models/invoiceItems.model')(sequelize, Sequelize);

// courier association
db.client.hasMany(db.courier, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});

db.train.hasMany(db.courier, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});

db.courier.belongsTo(db.train);
db.courier.belongsTo(db.client);

db.courier.hasMany(db.payment, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});

db.payment.belongsTo(db.courier);

db.client.hasMany(db.ledger);
db.ledger.belongsTo(db.client);

db.invoice.hasMany(db.invoiceItems, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});
db.invoiceItems.belongsTo(db.invoice);

module.exports = db;