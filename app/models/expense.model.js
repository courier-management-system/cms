module.exports = (sequelize, Sequelize) => {
    const Expense = sequelize.define("expenses", {
        type: {
            type: Sequelize.STRING,
            defaultValue: "others",
            allowNull: true
        },
        description: {
            type: Sequelize.STRING,
            allowNull: true
        },
        amount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        isDeleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
        }
    });

    return Expense;
};