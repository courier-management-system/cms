module.exports = (sequelize, Sequelize) => {
    const InvoiceItems = sequelize.define("invoiceItems", {
        vphNumber: {
            type: Sequelize.STRING,
            allowNull: false
        },
        consignor: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.STRING,
            allowNull: true
        },
        fromCity: {
            type: Sequelize.STRING,
            allowNull: false
        },
        toCity: {
            type: Sequelize.STRING,
            allowNull: false
        },
        weight: {
            type: Sequelize.STRING,
            allowNull: false
        },
        rate: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        courierDate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        amount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        }
    });

    return InvoiceItems;
};