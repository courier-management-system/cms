module.exports = (sequelize, Sequelize) => {
    const Invoice = sequelize.define("invoice", {
        invoiceNumber: {
            type: Sequelize.STRING,
            allowNull: false
        },
        customerName: {
            type: Sequelize.STRING,
            allowNull: true
        },
        customerGst: {
            type: Sequelize.STRING,
            allowNull: true
        },
        address: {
            type: Sequelize.STRING,
            allowNull: false
        },
        state: {
            type: Sequelize.STRING,
            allowNull: false
        },
        sgstPercentage: {
            type: Sequelize.DOUBLE,
            allowNull: true,
            defaultValue: 0
        },
        sgst: {
            type: Sequelize.DOUBLE,
            allowNull: true,
            defaultValue: 0
        },
        cgstPercentage: {
            type: Sequelize.DOUBLE,
            allowNull: true,
            defaultValue: 0
        },
        cgst: {
            type: Sequelize.DOUBLE,
            allowNull: true,
            defaultValue: 0
        },
        igstPercentage: {
            type: Sequelize.DOUBLE,
            allowNull: true,
            defaultValue: 0
        },
        igst: {
            type: Sequelize.DOUBLE,
            allowNull: true,
            defaultValue: 0
        },
        totalAmount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        }
    });

    return Invoice;
};


