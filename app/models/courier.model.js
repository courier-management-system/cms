module.exports = (sequelize, Sequelize) => {
    const Courier = sequelize.define("couriers", {
        vphNumber: {
            type: Sequelize.STRING,
            allowNull: false
        },
        packetNumber: {
            type: Sequelize.STRING,
            allowNull: true
        },
        description: {
            type: Sequelize.STRING,
            allowNull: true
        },
        type: {
            type: Sequelize.STRING,
            allowNull: true
        },
        weight: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        weightType: {
            type: Sequelize.STRING,
            allowNull: true
        },
        rate: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        courierDate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        totalAmount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        paidAmount: {
            type: Sequelize.DOUBLE,
            allowNull: true
        },
        balanceAmount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        paymentType: {
            type: Sequelize.STRING,
            allowNull: true
        },
        remark: {
            type: Sequelize.STRING,
            allowNull: true
        },
        isDeleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
        }
    });

    return Courier;
};