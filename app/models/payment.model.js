module.exports = (sequelize, Sequelize) => {
    const Payment = sequelize.define("payments", {
        totalAmount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        paidAmount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        balanceAmount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        remark: {
            type: Sequelize.STRING,
            allowNull: true
        },
        paymentDate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        isDeleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
        }
    });

    return Payment;
};