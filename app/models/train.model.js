module.exports = (sequelize, Sequelize) => {
    const Train = sequelize.define("trains", {
        trainNumber: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        toStation: {
            type: Sequelize.STRING,
            allowNull: false
        },
        fromStation: {
            type: Sequelize.STRING,
            allowNull: false
        },
        isDeleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
        }
    });

    return Train;
};