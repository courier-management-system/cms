module.exports = (sequelize, Sequelize) => {
    const Ledger = sequelize.define("ledgers", {
        creditAmount: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        remark: {
            type: Sequelize.STRING,
            allowNull: true
        },
        paymentDate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        isDeleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
        }
    });

    return Ledger;
};