module.exports = (sequelize, Sequelize) => {
    const Client = sequelize.define("clients", {
        companyName: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: true
        },
        contact: {
            type: Sequelize.STRING(10),
            allowNull: true
        },
        address: {
            type: Sequelize.STRING,
            allowNull: true
        },
        city: {
            type: Sequelize.STRING,
            allowNull: true
        },
        state: {
            type: Sequelize.STRING,
            allowNull: true
        },
        pincode: {
            type: Sequelize.STRING(6),
            allowNull: true
        },
        gstNo: {
            type: Sequelize.STRING,
            allowNull: true
        },
        isDeleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
        }
    });

    return Client;
};