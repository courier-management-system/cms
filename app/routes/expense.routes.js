const { authJwt } = require("../middlewares");
const controller = require("../controllers/expense.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(
        "/api/expense",
        [authJwt.verifyToken],
        controller.allExpenses
    );

    app.get(
        "/api/expense/:id",
        [authJwt.verifyToken],
        controller.getExpenseById
    );

    app.post(
        "/api/expense",
        [authJwt.verifyToken],
        controller.addExpense
    );

    app.put(
        "/api/expense/:id",
        [authJwt.verifyToken],
        controller.updateExpense
    );

    app.delete(
        "/api/expense/:id",
        [authJwt.verifyToken],
        controller.deleteExpense
    );

    app.put(
        "/api/expense",
        [authJwt.verifyToken],
        controller.restoreExpense
    );
};