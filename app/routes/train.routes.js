const { authJwt, TrainMiddleware } = require("../middlewares");
const controller = require("../controllers/train.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(
        "/api/train",
        [authJwt.verifyToken],
        controller.getTrains
    );

    app.get(
        "/api/train/:id",
        [authJwt.verifyToken],
        controller.getTrainById
    );

    app.post(
        "/api/train",
        [authJwt.verifyToken, TrainMiddleware.doesTrainExist],
        controller.addTrain
    );

    app.put(
        "/api/train/:id",
        [authJwt.verifyToken],
        controller.updateTrain
    );

    app.delete(
        "/api/train/:id",
        [authJwt.verifyToken],
        controller.deleteTrain
    );

    app.put(
        "/api/train",
        [authJwt.verifyToken],
        controller.restoreTrain
    );
}; 