const { authJwt, verifyCourierNumber } = require("../middlewares");
const controller = require("../controllers/orderMaster.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, Content-Type, Accept"
        );
        next();
    });

    app.post(
        "/api/order-master",
        [authJwt.verifyToken, verifyCourierNumber.checkDuplicateCourierNumber],
        controller.addOrderDetail
    );

    app.put(
        "/api/order-master",
        [authJwt.verifyToken],
        controller.updateOrderDetail
    );

};