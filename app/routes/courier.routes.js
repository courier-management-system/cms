const { authJwt } = require("../middlewares");
const controller = require("../controllers/courier.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(
        "/api/courier",
        [authJwt.verifyToken],
        controller.getAllCourierDetail
    );

    app.get(
        "/api/courier/:id",
        [authJwt.verifyToken],
        controller.getCourierDetailById
    );

    app.post(
        "/api/courier",
        [authJwt.verifyToken],
        controller.addCourierDetail
    );

    app.put(
        "/api/courier/:id",
        [authJwt.verifyToken],
        controller.updateCourierDetail
    );

    app.delete(
        "/api/courier/:id",
        [authJwt.verifyToken],
        controller.deleteCourierDetail
    );

    app.put(
        "/api/courier",
        [authJwt.verifyToken],
        controller.restoreCourierDetail
    );
};