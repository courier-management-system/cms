const { authJwt, verifyCompanyName } = require("../middlewares");
const controller = require("../controllers/client.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(
        "/api/client",
        [authJwt.verifyToken],
        controller.getClients
    );

    app.get(
        "/api/client/:id",
        [authJwt.verifyToken],
        controller.getClientById
    );

    app.post(
        "/api/client",
        [authJwt.verifyToken, verifyCompanyName.checkDuplicateCompanyName],
        controller.addClient
    );

    app.put(
        "/api/client/:id",
        [authJwt.verifyToken],
        controller.updateClient
    );

    app.delete(
        "/api/client/:id",
        [authJwt.verifyToken],
        controller.deleteClient
    );

    app.put(
        "/api/client",
        [authJwt.verifyToken],
        controller.restoreClient
    );
};