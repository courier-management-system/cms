const { authJwt, verifyInvoiceNumber } = require("../middlewares");
const controller = require("../controllers/invoice.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(
        "/api/invoice",
        [authJwt.verifyToken],
        controller.getInvoices
    );

    app.post(
        "/api/invoice",
        [authJwt.verifyToken, verifyInvoiceNumber.checkDuplicateInvoiceNumber],
        controller.addInvoice
    );

    app.put(
        "/api/invoice/:id",
        [authJwt.verifyToken],
        controller.updateInvoice
    );

    app.delete(
        "/api/invoice/:id",
        [authJwt.verifyToken],
        controller.deleteInvoice
    );
};