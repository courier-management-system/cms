require('dotenv').config();
const express = require("express");
const cors = require("cors");
const cookieSession = require("cookie-session");

const app = express();

app.use(cors());

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use(
    cookieSession({
        name: "bezkoder-session",
        secret: "COOKIE_SECRET", // should use as secret environment variable
        httpOnly: true
    })
);

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/client.routes')(app);
require('./app/routes/train.routes')(app);
require('./app/routes/courier.routes')(app);
require('./app/routes/payment.routes')(app);
require('./app/routes/expense.routes')(app);
require('./app/routes/orderMaster.routes')(app);
require('./app/routes/invoice.routes')(app);



const db = require("./app/models");

db.sequelize.sync();
// db.sequelize.sync({ force: true });

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
